CREATE DATABASE IF NOT EXISTS jugueteriahibernate;
USE jugueteriahibernate;

CREATE TABLE IF NOT EXISTS cliente 
(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(50),
	nombre VARCHAR(50),
	direccion VARCHAR(50),
	telefono VARCHAR(20)
);
CREATE TABLE IF NOT EXISTS juguetes 
(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	codigo VARCHAR(50) UNIQUE NOT NULL,
	nombre VARCHAR(50) NOT NULL,
	descripcion VARCHAR(50),
	marca VARCHAR(20) NOT NULL,
    precio FLOAT DEFAULT 0,
    anyo_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP()
);
CREATE TABLE IF NOT EXISTS jugueteria 
(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(50) NOT NULL,
	ciudad VARCHAR(50)NOT NULL,
	pais VARCHAR(20) NOT NULL,
    telefono VARCHAR(9) NOT NULL
);
CREATE TABLE IF NOT EXISTS empleados 
(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nif VARCHAR(10) NOT NULL UNIQUE,
	nombre VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    turno VARCHAR(30) NOT NULL,
	direccion VARCHAR(50)NOT NULL,
	cargo VARCHAR(20) NOT NULL,
    telefono VARCHAR(9) NOT NULL,
    id_encargado INT UNSIGNED REFERENCES encargado
);
CREATE TABLE IF NOT EXISTS encargado 
(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nif VARCHAR(10) NOT NULL UNIQUE,
	nombre VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    anyos_cargo INT,
    edad INT,
    telefono VARCHAR(9) NOT NULL
);
CREATE TABLE IF NOT EXISTS proveedor 
(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    codigo_proveedor VARCHAR(50) UNIQUE NOT NULL,
	nombre VARCHAR(50) NOT NULL,
    pais VARCHAR(40) NOT NULL,
    ciudad VARCHAR(40) NOT NULL,
    direccion VARCHAR(50) NOT NULL,
    cantidad FLOAT DEFAULT 0,
    telefono VARCHAR(9) NOT NULL
);
CREATE TABLE IF NOT EXISTS cliente_juguetes
(
	id_cliente INT UNSIGNED REFERENCES cliente,
	id_juguetes INT UNSIGNED REFERENCES juguetes,
	fecha_compra TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id_cliente, id_juguetes)
);
CREATE TABLE IF NOT EXISTS cliente_jugueteria
(
	id_cliente INT UNSIGNED REFERENCES cliente,
	id_jugueteria INT UNSIGNED REFERENCES jugueteria,
	fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id_cliente, id_jugueteria)
);
CREATE TABLE IF NOT EXISTS jugueteria_juguetes
(
	id_jugueteria INT UNSIGNED REFERENCES jugueteria,
	id_juguetes INT UNSIGNED REFERENCES juguetes,
	PRIMARY KEY (id_jugueteria, id_juguetes)
);
CREATE TABLE IF NOT EXISTS jugueteria_proveedor
(
	id_jugueteria INT UNSIGNED REFERENCES jugueteria,
	id_proveedor INT UNSIGNED REFERENCES proveedor,
	PRIMARY KEY (id_jugueteria, id_proveedor)
);
CREATE TABLE IF NOT EXISTS jugueteria_empleados
(
	id_jugueteria INT UNSIGNED REFERENCES jugueteria,
	id_empleados INT UNSIGNED REFERENCES empleados,
	PRIMARY KEY (id_jugueteria, id_empleados)
);
CREATE TABLE IF NOT EXISTS juguetes_empleados
(
	id_juguetes INT UNSIGNED REFERENCES juguetes,
	id_empleados INT UNSIGNED REFERENCES empleados,
    fecha_venta TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id_juguetes, id_empleados)
);




