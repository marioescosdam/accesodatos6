package com.marioea.hibernate.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;


/**
 * creamos  la tabla jugueteria con sus respectivas variables que se encuentran en la bbdd
 */
@Entity
public class Proveedor {
    private int id;
    private String codigoProveedor;
    private String nombre;
    private String pais;
    private String ciudad;
    private String direccion;
    private double cantidad;
    private String telefono;
    private List<Jugueteria> jugueterias;

    /**
     * generamos el getter de id
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * generamos el setter de id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * gneramos el getter de codigo_proveedor
     * @return
     */
    @Basic
    @Column(name = "codigo_proveedor")
    public String getCodigoProveedor() {
        return codigoProveedor;
    }

    /**
     * generamos el setter de codigoProveedor
     * @param codigoProveedor
     */
    public void setCodigoProveedor(String codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
    }

    /**
     * generamos el getter de nombre
     * @return
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    /**
     * generamos el setter de nombre
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * generamos el getter de pais
     * @return
     */
    @Basic
    @Column(name = "pais")
    public String getPais() {
        return pais;
    }

    /**
     * generamos el setter de pais
     * @param pais
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * gneramos el getter de ciudad
     * @return
     */
    @Basic
    @Column(name = "ciudad")
    public String getCiudad() {
        return ciudad;
    }

    /**
     * generamos el setter de ciudad
     * @param ciudad
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * generamos el getter de direcciom
     * @return
     */
    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    /**
     * generamos el setter de direccion
     * @param direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * generamos el getter de cantidad
     * @return
     */
    @Basic
    @Column(name = "cantidad")
    public double getCantidad() {
        return cantidad;
    }

    /**
     * generamos el setter de cantidad
     * @param cantidad
     */
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * generamos el getter de telefono
     * @return
     */
    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    /**
     * generamos el setter de telefono
     * @param telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    /**
     * te permite saber si dos objetos son iguales
     *
     * @param o objeto introducido para comprobar si es igual
     * @return true si es igual
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proveedor proveedor = (Proveedor) o;
        return id == proveedor.id &&
                Double.compare(proveedor.cantidad, cantidad) == 0 &&
                Objects.equals(codigoProveedor, proveedor.codigoProveedor) &&
                Objects.equals(nombre, proveedor.nombre) &&
                Objects.equals(pais, proveedor.pais) &&
                Objects.equals(ciudad, proveedor.ciudad) &&
                Objects.equals(direccion, proveedor.direccion) &&
                Objects.equals(telefono, proveedor.telefono);
    }

    /**
     * te hasea un objeto
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, codigoProveedor, nombre, pais, ciudad, direccion, cantidad, telefono);
    }
    /**
     * La siguiente relacion que vamos a realizar entre proovedor y jugueteria en este caso es una relacion(N,M)
     * se realizaria automaticamente la relacion a la hora de mapear.
     * Tanto jugueteria y proveedor son varios no es solo uno.
     * @return
     */
    @ManyToMany(mappedBy = "proveedores")
    public List<Jugueteria> getJugueterias() {
        return jugueterias;
    }

    public void setJugueterias(List<Jugueteria> jugueterias) {
        this.jugueterias = jugueterias;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", pais='" + pais + '\'' +
                ", ciudad='" + ciudad + '\'' +
                ", direccion='" + direccion + '\'' +
                ", cantidad=" + cantidad;


    }
}
