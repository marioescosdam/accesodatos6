package com.marioea.hibernate.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

/**
 * creamos la tabla cliente_juguetes con
 * los datos que aparecen registrados en la base de datos
 */
@Entity
@Table(name = "cliente_juguetes", schema = "jugueteriahibernate", catalog = "")
public class ClienteJuguete {
    private int id;
    private Date fechaCompra;
    private Cliente clientes;
    private Juguete juguetes;

    /**
     * generamos el getter de id
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * generamos el setter de id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * generamos el getter de fecha_compra
     * @return
     */
    @Basic
    @Column(name = "fecha_compra")
    public Date getFechaCompra() {
        return fechaCompra;
    }

    /**
     * generamos el setter de fecha_compra
     * @param fechaCompra
     */
    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    /**
     * te permite saber si dos objetos son iguales
     *
     * @param o objeto introducido para comprobar si es igual
     * @return true si es igual
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClienteJuguete that = (ClienteJuguete) o;
        return id == that.id &&
                Objects.equals(fechaCompra, that.fechaCompra);
    }

    /**
     * te hasea un objeto
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, fechaCompra);
    }

    /**
     *En esta relación que hacemos es la tabla cliente_juguete con la de cliente en este caso es una relación (N,1)
     *la de cliente que seria el 1 a N seria cliente_juguete.Basicamente aqui recogeriamos la relacion que existe entres cliente y cliente_juguete.
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id", nullable = false)
    public Cliente getClientes() {
        return clientes;
    }

    public void setClientes(Cliente clientes) {
        this.clientes = clientes;
    }
    /**
     *En esta relación que hacemos es la tabla cliente_juguete con la de juguetes en este caso es una relación (N,1)
     * la de juguetes que seria el 1 a N seria cliente_juguete.Basicamente aqui recogeriamos la relacion que existe entres juguetes y cliente_juguete.
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "id_juguetes", referencedColumnName = "id", nullable = false)
    public Juguete getJuguetes() {
        return juguetes;
    }

    public void setJuguetes(Juguete juguetes) {
        this.juguetes = juguetes;
    }
}
