package com.marioea.hibernate.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

/**
 * creamos la tabla cliente_jugueteria con sus variables como id, fecha,cliente y jugueteria
 */
@Entity
@Table(name = "cliente_jugueteria", schema = "jugueteriahibernate", catalog = "")
public class ClienteJugueteria {
    private int id;
    private Date fecha;
    private Cliente cliente;
    private Jugueteria jugueteria;

    /**
     * getter de id
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * setter de id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * getter de fecha
     * @return
     */
    @Basic
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    /**
     * setter de fecha
     * @param fecha
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * te permite saber si dos objetos son iguales
     *
     * @param o objeto introducido para comprobar si es igual
     * @return true si es igual
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClienteJugueteria that = (ClienteJugueteria) o;
        return id == that.id &&
                Objects.equals(fecha, that.fecha);
    }
    /**
     * te hasea un objeto
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, fecha);
    }


    /**
     *En esta relación que hacemos es la tabla cliente_jugueteria con la de cliente en este caso es una relación (N,1)
     *la de cliente que seria el 1 a N seria cliente_jugueteria.Basicamente aqui recogeriamos la relacion que existe entres cliente y cliente_jugueteria.
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id", nullable = false)
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    /**
     *En esta relación que hacemos es la tabla cliente_jugueteria con la de jugueteria en este caso es una relación (N,1)
     *la de cliente que seria el 1 a N seria cliente_jugueteria.Basicamente aqui recogeriamos la relacion que existe entre jugueteria y cliente_jugueteria.
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "id_jugueteria", referencedColumnName = "id", nullable = false)
    public Jugueteria getJugueteria() {
        return jugueteria;
    }

    public void setJugueteria(Jugueteria jugueteria) {
        this.jugueteria = jugueteria;
    }
}
