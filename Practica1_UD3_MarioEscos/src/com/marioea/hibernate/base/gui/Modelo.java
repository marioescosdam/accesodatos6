package com.marioea.hibernate.base.gui;

import com.marioea.hibernate.base.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

public class Modelo {
    SessionFactory sessionFactory;

    /**
     * Metodo que sirve para cerrar la conexion con nuestra base de datos
     */
    public void desconectar() {
        //Cierro la factoria de sessiones
        if (sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Metodo que sirve para establecer una conexion con nuestra base de datos
     */
    public void conectar() {
        Configuration configuracion = new Configuration();
        //Cargo el fichero Hibernate.cfg.xml
        configuracion.configure("hibernate.cfg.xml");

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(ClienteJuguete.class);
        configuracion.addAnnotatedClass(ClienteJugueteria.class);
        configuracion.addAnnotatedClass(Empleado.class);
        configuracion.addAnnotatedClass(Encargado.class);
        configuracion.addAnnotatedClass(Juguete.class);
        configuracion.addAnnotatedClass(JugueteEmpleado.class);
        configuracion.addAnnotatedClass(Jugueteria.class);
        configuracion.addAnnotatedClass(Proveedor.class);


        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);

    }

    /**
     * Obtenemos una session la cual utilizaamos para llamar a la clase cliente y dar de alta a un cliente
     * @param nuevoCliente
     */
    public void altaCliente(Cliente nuevoCliente) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoCliente);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y creamos una consulta para coger la tabla cliente
     * Creo un array list para almacenar los resultados obtenidos y cierro la sesion y retorno el array con los datos almacenados
     * @return
     */
    public ArrayList<Cliente> getCliente() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente ");
        ArrayList<Cliente> lista = (ArrayList<Cliente>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y realizamos un modificacion y luego mediante el update actualizamos la tabla
     * @param clienteSeleccion
     */
    public void modificarCliente(Cliente clienteSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(clienteSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y elimino un cliente de la lista y cierro la session
     * @param clienteBorrado
     */
    public void borrarCliente(Cliente clienteBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(clienteBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     *  Obtenemos una session la cual utilizamos para llamar a la clase empleado y dar de alta a un empleado
     * @param nuevoEmpleado
     */
    public void altaEmpleado(Empleado nuevoEmpleado) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoEmpleado);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y creamos una consulta para coger la tabla empleado
     * Creo un array list para almacenar los resultados obtenidos y cierro la sesion y retorno el array con los datos almacenados
     * @return
     */
    public ArrayList<Empleado> getEmpleado() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Empleado ");
        ArrayList<Empleado> lista = (ArrayList<Empleado>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y realizamos un modificacion y luego mediante el update actualizamos la tabla
     * @param empleadoSeleccion
     */
    public void modificarEmpleado(Empleado empleadoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(empleadoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y elimino un empleado de la lista y cierro la session
     * @param empleadoBorrado
     */
    public void borrarEmpleado(Empleado empleadoBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(empleadoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }


    /**
     * Obtenemos una session la cual utilizaamos para llamar a la clase encargado y dar de alta a un encargado
     * @param nuevoEncargado
     */
    public void altaEncargado(Encargado nuevoEncargado) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoEncargado);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y creamos una consulta para coger la tabla encargado
     * Creo un array list para almacenar los resultados obtenidos y cierro la sesion y retorno el array con los datos almacenados
     * @return
     */
    public ArrayList<Encargado> getEncargado() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Encargado ");
        ArrayList<Encargado> lista = (ArrayList<Encargado>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y realizamos un modificacion y luego mediante el update actualizamos la tabla
     * @param encargadoSeleccion
     */
    public void modificarEncargado(Encargado encargadoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(encargadoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y elimino un encargado de la lista y cierro la session
     * @param encargadoBorrado
     */
    public void borrarEncargado(Encargado encargadoBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(encargadoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Obtenemos una session la cual utilizaamos para llamar a la clase encargado y dar de alta a un juguete
     * @param nuevoJuguete
     */
    public void altaJuguete(Juguete nuevoJuguete) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoJuguete);
        sesion.getTransaction().commit();

        sesion.close();
    }
    public ArrayList<Juguete> getJuguete() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Juguete ");
        ArrayList<Juguete> lista = (ArrayList<Juguete>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y realizamos un modificacion y luego mediante el update actualizamos la tabla
     * @param jugueteSeleccion
     */
    public void modificarJuguete(Juguete jugueteSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(jugueteSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y elimino un juguete de la lista y cierro la session
     * @param jugueteBorrado
     */
    public void borrarJuguete(Juguete jugueteBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(jugueteBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Obtenemos una session la cual utilizaamos para llamar a la clase jugueteria y dar de alta a una jugueteria
     * @param nuevaJugueteria
     */
    public void altaJugueteria(Jugueteria nuevaJugueteria) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaJugueteria);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y creamos una consulta para coger la tabla jugueteria
     * Creo un array list para almacenar los resultados obtenidos y cierro la sesion y retorno el array con los datos almacenados
     * @return
     */
    public ArrayList<Jugueteria> getJugueteria() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Jugueteria ");
        ArrayList<Jugueteria> lista = (ArrayList<Jugueteria>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y realizamos un modificacion y luego mediante el update actualizamos la tabla
     * @param jugueteriaSeleccion
     */
    public void modificarJugueteria(Jugueteria jugueteriaSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(jugueteriaSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     *  Obtengo una session a partir de la factoria de sesiones y elimino un juguete de la lista y cierro la session
     * @param jugueteriaBorrado
     */
    public void borrarJugueteria(Jugueteria jugueteriaBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(jugueteriaBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Obtenemos una session la cual utilizaamos para llamar a la clase proveedor y dar de alta a una proveedor
     * @param nuevoProveedor
     */
    public void altaProveedor(Proveedor nuevoProveedor) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoProveedor);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y creamos una consulta para coger la tabla proveedor
     * Creo un array list para almacenar los resultados obtenidos y cierro la sesion y retorno el array con los datos almacenados
     * @return
     */
    public ArrayList<Proveedor> getProveedor() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Proveedor ");
        ArrayList<Proveedor> lista = (ArrayList<Proveedor>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y realizamos un modificacion y luego mediante el update actualizamos la tabla
     * @param proveedorSeleccion
     */
    public void modificarProveedor(Proveedor proveedorSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(proveedorSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Obtengo una session a partir de la factoria de sesiones y elimino un juguete de la lista y cierro la session
     * @param proveedorBorrado
     */
    public void borrarProveedor(Proveedor proveedorBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(proveedorBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }


    public ArrayList<Cliente> getClienteJuguete(Juguete jugueteSeleccionado) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente WHERE jugueteSeleccionado = :prop");
        query.setParameter("prop", jugueteSeleccionado);
        ArrayList<Cliente> lista = (ArrayList<Cliente>) query.getResultList();
        sesion.close();
        return lista;
    }
    public ArrayList<Cliente> getClienteJugueteria(Jugueteria jugueteriaSeleccionada) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente WHERE jugueteriaSeleccionada = :prop");
        query.setParameter("prop", jugueteriaSeleccionada);
        ArrayList<Cliente> lista = (ArrayList<Cliente>) query.getResultList();
        sesion.close();
        return lista;
    }
    public ArrayList<Juguete> getJugueteEmpleado(Juguete jugueteSeleccionado) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Juguete WHERE jugueteriaSeleccionado = :prop");
        query.setParameter("prop", jugueteSeleccionado);
        ArrayList<Juguete> lista = (ArrayList<Juguete>) query.getResultList();
        sesion.close();
        return lista;
    }
    public ArrayList<Empleado> getEmpleadoEncargado(Encargado encargadoSeleccionado) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Empleado WHERE encargadoSeleccionado = :prop");
        query.setParameter("prop", encargadoSeleccionado);
        ArrayList<Empleado> lista = (ArrayList<Empleado>) query.getResultList();
        sesion.close();
        return lista;
    }

}