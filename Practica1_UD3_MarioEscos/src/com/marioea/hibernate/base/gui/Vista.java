package com.marioea.hibernate.base.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import java.awt.*;

/**
 * Vista que contiene todos los botones, ventanas y etc.. de nuestra intefaz de nuestra aplicacion
 */
public class Vista extends Frame{
    private JFrame frame;
    private JPanel panel1;
    JTabbedPane tabbedPane1;
    JTextField txtIdCliente;
    JTextField txtNombreCliente;
    JTextField txtEmailCliente;
    JTextField txtDireccionCliente;
    JTextField txtTelefonoCliente;
    JButton btnAltaCliente;
    JButton btnModificarCliente;
    JButton btnBorrarCliente;
    JButton btnListarCliente;
    JList listClientes;
    JTextField txtIdEmpleado;
    JTextField txtNifEmpleado;
    JTextField txtNombreEmpleado;
    JTextField txtApellidosEmpleado;
    JTextField txtTurnoEmpleado;
    JTextField txtDireccionEmpleado;
    JTextField txtCargoEmpleado;
    JTextField txtTelefonoEmpleado;
    JTextField txtIdEncargado;
    JTextField txtNifEncargado;
    JTextField txtNombreEncargado;
    JTextField txtApellidosEncargado;
    JTextField txtAniosEncargado;
    JTextField txtEdadEncargado;
    JTextField txtTelefonoEncargado;
    JButton altaEncargadoButton;
    JButton listarEncargadosButton;
    JButton modificarEncargadoButton;
    JButton borrarEncargadoButton;
    JList listEncargado;
    JTextField txtIdJuguete;
    JTextField txtCodigoJuguete;
    JTextField txtNombreJuguete;
    JTextField txtDescripcionJuguete;
    JTextField txtMarcaJuguete;
    JTextField txtPrecioJuguete;
    DateTimePicker dateTimePicker;
    JButton btnAltaJuguete;
    JButton btnlistarJuguetes;
    JButton btnmodificarJuguete;
    JButton btnborrarJuguete;
    JList listJuguete;
    JTextField txtIdJugueteria;
    JTextField txtNombreJugueteria;
    JTextField txtTelefonoJugueteria;
    JButton btnaltaJugueteria;
    JButton btnlistarJuguterias;
    JButton btnmodificarJugueteria;
    JButton btnborrarJugueteria;
    JList listJugueteria;
    JTextField txtIdProveedor;
    JTextField txtCodigoProveedor;
    JTextField txtNombreProveedor;
    JTextField txtPaisProveedor;
    JTextField txtCiudadProveedor;
    JTextField txtDireccionProveedor;
    JTextField txtCantidadProveedor;
    JTextField txtTelefonoProveedor;
    JButton btnaltaProveedor;
    JButton btnlistarProveedor;
    JButton btnmodificarProveedor;
    JButton borrarProveedorButton;
    JList listProveedor;
    JTextField txtCiudadJugueteria;
    JTextField txtPaisJugueteria;
    JList listClienteJuguete;
    JList listClienteJugueteria;
    JButton btnaltaEmpleado;
    JButton btnmodificarEmpleado;
    JButton btnborrarEmpleado;
    JButton btnlistarEmpleado;
    JList listEmpleado;
    JButton btnlistarClienteJuguete;
    JButton btnlistarClienteJugueteria;
    DatePicker fecha;


    DefaultListModel dlmCliente;

    DefaultListModel dlmEmpleado;
    DefaultListModel dlmEncargado;
    DefaultListModel dlmJuguete;


    DefaultListModel dlmJugueteria;
    DefaultListModel dlmProveedor;

    JMenuItem conexionItem;
    JMenuItem salirItem;

    /**
     * Configuramos la ventana
     */
    public Vista(){
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+300, this.getHeight()+800));
        frame.setLocationRelativeTo(null);

        crearMenu();
        crearModelos();

    }

    /**
     * Le damos a las listar creadas un default list model
     */
    private void crearModelos() {
        dlmCliente = new DefaultListModel();
        listClientes.setModel(dlmCliente);
        dlmEmpleado = new DefaultListModel();
        listEmpleado.setModel(dlmEmpleado);
        dlmEncargado = new DefaultListModel();
        listEncargado.setModel(dlmEncargado);
        dlmJuguete = new DefaultListModel();
        listJuguete.setModel(dlmJuguete);
        dlmJugueteria = new DefaultListModel();
        listJugueteria.setModel(dlmJugueteria);
        dlmProveedor = new DefaultListModel();
        listProveedor.setModel(dlmProveedor);
    }

    /**
     * Creamos un menu desplegable para cuando le demos a archivo nos despliegue dos opcion Conectar o Salir
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }
}
