package com.marioea.hibernate.base.gui;

import com.marioea.hibernate.base.*;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;

import java.util.ArrayList;

public class Controlador implements ActionListener, ListSelectionListener {
    private Vista vista;
    private Modelo modelo;

    /**
     * hacemos un constructor de controlador con la vista y el modelo
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListener(this);
    }

    /**
     * cremos un switch con las opciones de cada boton
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando) {
            //accedemos al metodo del modelo para desconectar nuestro programa
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;
                //hacemos uso del item de conectar del menu desplegable y llamamos al conectar de modelo
            // para establecer la conexion con la bbdd
            case "Conectar":
                vista.conexionItem.setEnabled(false);
                modelo.conectar();
                break;
            //damos de alta a un cliente rellenando cada uno de sus textfields
            // y llamamos al metodo del modelo y de esta manera damos de alta
            case "Alta Cliente":
                Cliente nuevoCliente = new Cliente();
                nuevoCliente.setId(Integer.parseInt(vista.txtIdCliente.getText()));
                nuevoCliente.setNombre(vista.txtNombreCliente.getText());
                nuevoCliente.setEmail(vista.txtEmailCliente.getText());
                nuevoCliente.setDireccion(vista.txtDireccionCliente.getText());
                nuevoCliente.setTelefono(vista.txtTelefonoCliente.getText());
                modelo.altaCliente(nuevoCliente);
                break;
            //llamamos al listar de modelo y este unicamente nos muestra todos los datos en el list
            case "Listar Cliente":
                listarCliente(modelo.getCliente());
                break;
            //modificamos un cliente para ello cogemos todos los datos de sus textfields
            // y llamamos al metodo de modelo y modificamos el dato que deseamos reemplazar
            case "Modificar Cliente":
                Cliente clienteSeleccion = (Cliente) vista.listClientes.getSelectedValue();
                clienteSeleccion.setId(Integer.parseInt(vista.txtIdCliente.getText()));
                clienteSeleccion.setNombre(vista.txtNombreCliente.getText());
                clienteSeleccion.setEmail(vista.txtEmailCliente.getText());
                clienteSeleccion.setDireccion(vista.txtDireccionCliente.getText());
                clienteSeleccion.setTelefono(vista.txtTelefonoCliente.getText());
                modelo.modificarCliente(clienteSeleccion);
                break;
            //accedemos al metdodo de modelo y seleccionamos la fila de la lista
            // que deseamos borrar la borramos y listamos y ha desaparecido
            case "Borrar Cliente":
                Cliente clienteBorrado = (Cliente) vista.listClientes.getSelectedValue();
                modelo.borrarCliente(clienteBorrado);
                break;

            case "Alta Empleado":
                Empleado nuevoEmpleado = new Empleado();
                nuevoEmpleado.setId(Integer.parseInt(vista.txtIdEmpleado.getText()));
                nuevoEmpleado.setNif(vista.txtNifEmpleado.getText());
                nuevoEmpleado.setNombre(vista.txtNombreEmpleado.getText());
                nuevoEmpleado.setApellidos(vista.txtApellidosEmpleado.getText());
                nuevoEmpleado.setTurno(vista.txtTurnoEmpleado.getText());
                nuevoEmpleado.setDireccion(vista.txtDireccionEmpleado.getText());
                nuevoEmpleado.setCargo(vista.txtCargoEmpleado.getText());
                nuevoEmpleado.setTelefono(vista.txtTelefonoEmpleado.getText());
                modelo.altaEmpleado(nuevoEmpleado);
                break;

            case "Listar Empleado":
                listarEmpleado(modelo.getEmpleado());
                break;

            case "Modificar Empleado":
                Empleado empleadoSeleccion = (Empleado) vista.listEmpleado.getSelectedValue();
                empleadoSeleccion.setId(Integer.parseInt(vista.txtIdEmpleado.getText()));
                empleadoSeleccion.setNif(vista.txtNifEmpleado.getText());
                empleadoSeleccion.setNombre(vista.txtNombreEmpleado.getText());
                empleadoSeleccion.setApellidos(vista.txtApellidosEmpleado.getText());
                empleadoSeleccion.setTurno(vista.txtTurnoEmpleado.getText());
                empleadoSeleccion.setDireccion(vista.txtDireccionEmpleado.getText());
                empleadoSeleccion.setCargo(vista.txtCargoEmpleado.getText());
                empleadoSeleccion.setTelefono(vista.txtTelefonoEmpleado.getText());
                modelo.modificarEmpleado(empleadoSeleccion);
                break;

            case "Borrar Empleado":
                Empleado empleadoborrado = (Empleado) vista.listEmpleado.getSelectedValue();
                modelo.borrarEmpleado(empleadoborrado);
                break;

            case "Alta Encargado":
                Encargado nuevoEncargado = new Encargado();
                nuevoEncargado.setId(Integer.parseInt(vista.txtIdEncargado.getText()));
                nuevoEncargado.setNif(vista.txtNifEncargado.getText());
                nuevoEncargado.setNombre(vista.txtNombreEncargado.getText());
                nuevoEncargado.setApellidos(vista.txtApellidosEncargado.getText());
                nuevoEncargado.setAnyosCargo(Integer.parseInt(vista.txtAniosEncargado.getText()));
                nuevoEncargado.setEdad(Integer.parseInt(vista.txtEdadEncargado.getText()));
                nuevoEncargado.setTelefono(vista.txtTelefonoEncargado.getText());
                modelo.altaEncargado(nuevoEncargado);
                break;

            case "Listar Encargados":
                listarEncargado(modelo.getEncargado());
                break;

            case "Modificar Encargado":
                Encargado encargadoSeleccion = (Encargado) vista.listEncargado.getSelectedValue();
                encargadoSeleccion.setId(Integer.parseInt(vista.txtIdEncargado.getText()));
                encargadoSeleccion.setNif(vista.txtNifEncargado.getText());
                encargadoSeleccion.setNombre(vista.txtNombreEncargado.getText());
                encargadoSeleccion.setApellidos(vista.txtApellidosEncargado.getText());
                encargadoSeleccion.setAnyosCargo(Integer.parseInt(vista.txtAniosEncargado.getText()));
                encargadoSeleccion.setEdad(Integer.parseInt(vista.txtEdadEncargado.getText()));
                encargadoSeleccion.setTelefono(vista.txtTelefonoEncargado.getText());
                modelo.modificarEncargado(encargadoSeleccion);
                break;

            case "Borrar Encargado":
                Encargado encargadoborrado = (Encargado) vista.listEncargado.getSelectedValue();
                modelo.borrarEncargado(encargadoborrado);
                break;
            case "Alta Juguete":
                Juguete nuevoJuguete = new Juguete();
                nuevoJuguete.setId(Integer.parseInt(vista.txtIdJuguete.getText()));
                nuevoJuguete.setCodigo(vista.txtCodigoJuguete.getText());
                nuevoJuguete.setNombre(vista.txtNombreJuguete.getText());
                nuevoJuguete.setDescripcion(vista.txtDescripcionJuguete.getText());
                nuevoJuguete.setMarca(vista.txtMarcaJuguete.getText());
                nuevoJuguete.setPrecio(Integer.parseInt(vista.txtPrecioJuguete.getText()));
                nuevoJuguete.setAnyoCreacion(Date.valueOf(vista.fecha.getDate()));
                modelo.altaJuguete(nuevoJuguete);
                break;

            case "Listar Juguetes":
                listarJuguete(modelo.getJuguete());
                break;

            case "Modificar Juguete":
                Juguete jugueteSeleccion = (Juguete) vista.listJuguete.getSelectedValue();
                jugueteSeleccion.setId(Integer.parseInt(vista.txtIdJuguete.getText()));
                jugueteSeleccion.setCodigo(vista.txtCodigoJuguete.getText());
                jugueteSeleccion.setNombre(vista.txtNombreJuguete.getText());
                jugueteSeleccion.setDescripcion(vista.txtDescripcionJuguete.getText());
                jugueteSeleccion.setMarca(vista.txtMarcaJuguete.getText());
                jugueteSeleccion.setPrecio(Double.parseDouble(vista.txtPrecioJuguete.getText()));
                jugueteSeleccion.setAnyoCreacion(Date.valueOf(String.valueOf(vista.fecha.getDate())));
                modelo.modificarJuguete(jugueteSeleccion);
                break;

            case "Borrar Juguete":
                Juguete jugueteborrado = (Juguete) vista.listJuguete.getSelectedValue();
                modelo.borrarJuguete(jugueteborrado);
                break;
            case "Alta Jugueteria":
                Jugueteria nuevoJugueteria = new Jugueteria();
                nuevoJugueteria.setId(Integer.parseInt(vista.txtIdJugueteria.getText()));
                nuevoJugueteria.setNombre(vista.txtNombreJugueteria.getText());
                nuevoJugueteria.setCiudad(vista.txtCiudadJugueteria.getText());
                nuevoJugueteria.setPais(vista.txtPaisJugueteria.getText());
                nuevoJugueteria.setTelefono(vista.txtTelefonoJugueteria.getText());
                modelo.altaJugueteria(nuevoJugueteria);
                break;

            case "Listar Jugueterias":
                listarJugueteria(modelo.getJugueteria());
                break;

            case "Modificar Jugueteria":
                Jugueteria jugueteriaSeleccion = (Jugueteria) vista.listJugueteria.getSelectedValue();
                jugueteriaSeleccion.setId(Integer.parseInt(vista.txtIdJugueteria.getText()));
                jugueteriaSeleccion.setNombre(vista.txtNombreJugueteria.getText());
                jugueteriaSeleccion.setCiudad(vista.txtCiudadJugueteria.getText());
                jugueteriaSeleccion.setPais(vista.txtPaisJugueteria.getText());
                jugueteriaSeleccion.setTelefono(vista.txtTelefonoJugueteria.getText());
                modelo.modificarJugueteria(jugueteriaSeleccion);
                break;

            case "Borrar Jugueteria":
                Jugueteria jugueteriaborrado = (Jugueteria) vista.listJugueteria.getSelectedValue();
                modelo.borrarJugueteria(jugueteriaborrado);
                break;
            case "Alta Proveedor":
                Proveedor nuevoProveedor = new Proveedor();
                nuevoProveedor.setId(Integer.parseInt(vista.txtIdProveedor.getText()));
                nuevoProveedor.setCodigoProveedor(vista.txtCodigoProveedor.getText());
                nuevoProveedor.setNombre(vista.txtNombreProveedor.getText());
                nuevoProveedor.setPais(vista.txtPaisProveedor.getText());
                nuevoProveedor.setCiudad(vista.txtCiudadProveedor.getText());
                nuevoProveedor.setDireccion(vista.txtDireccionProveedor.getText());
                nuevoProveedor.setCantidad(Double.parseDouble(vista.txtCantidadProveedor.getText()));
                nuevoProveedor.setTelefono(vista.txtTelefonoProveedor.getText());
                modelo.altaProveedor(nuevoProveedor);
                break;

            case "Listar Proveedor":
                listarProveedor(modelo.getProveedor());
                break;

            case "Modificar Proveedor":
                Proveedor proveedorSeleccion = (Proveedor) vista.listProveedor.getSelectedValue();
                proveedorSeleccion.setId(Integer.parseInt(vista.txtIdProveedor.getText()));
                proveedorSeleccion.setCodigoProveedor(vista.txtCodigoProveedor.getText());
                proveedorSeleccion.setNombre(vista.txtNombreProveedor.getText());
                proveedorSeleccion.setPais(vista.txtPaisProveedor.getText());
                proveedorSeleccion.setCiudad(vista.txtCiudadProveedor.getText());
                proveedorSeleccion.setDireccion(vista.txtDireccionProveedor.getText());
                proveedorSeleccion.setCantidad(Double.parseDouble(vista.txtCantidadProveedor.getText()));
                proveedorSeleccion.setTelefono(vista.txtTelefonoProveedor.getText());
                modelo.modificarProveedor(proveedorSeleccion);
                break;

            case "Borrar Proveedor":
                Proveedor proveedorborrado = (Proveedor) vista.listProveedor.getSelectedValue();
                modelo.borrarProveedor(proveedorborrado);
                break;

        }


    }

    /**
     * Creamos un array de jugueterias limpiamos el default list model ç
     * y añadimos elementos a ese default list miodel
     * Su funcionalidad principal es para listar todos los datos de jugueterias en la lista de la pestaña de jugueteria
     * @param jugueterias
     */
    private void listarJugueteria(ArrayList<Jugueteria> jugueterias) {
        vista.dlmJugueteria.clear();
        for(Jugueteria jugueteria : jugueterias){
            vista.dlmJugueteria.addElement(jugueteria);
        }
    }

    /**
     * Creamos un array de clientes limpiamos el default list model ç
     * y añadimos elementos a ese default list miodel
     * Su funcionalidad principal es para listar todos los datos de clientes en la lista de la pestaña de clientes
     * @param lista
     */
    public void listarCliente(ArrayList<Cliente> lista){
        vista.dlmCliente.clear();
        for(Cliente uncliente : lista){
            vista.dlmCliente.addElement(uncliente);
        }
    }

    /**
     * Creamos un array de proveedor limpiamos el default list model ç
     *  y añadimos elementos a ese default list miodel
     *  Su funcionalidad principal es para listar todos los datos de proovedor en la lista de la pestaña de proovedor
     * @param lista
     */
    public void listarProveedor(ArrayList<Proveedor> lista){
        vista.dlmProveedor.clear();
        for(Proveedor unproveedor : lista){
            vista.dlmProveedor.addElement(unproveedor);
        }
    }

    /**
     * Creamos un array de juguete limpiamos el default list model
     * y añadimos elementos a ese default list miodel
     * Su funcionalidad principal es para listar todos los datos de juguete en la lista de la pestaña de juguete
     * @param lista
     */
    public void listarJuguete(ArrayList<Juguete> lista){
        vista.dlmJuguete.clear();
        for(Juguete unjuguete : lista){
            vista.dlmJuguete.addElement(unjuguete);
        }
    }

    /**
     * Creamos un array de empleado limpiamos el default list model
     * y añadimos elementos a ese default list miodel
     * Su funcionalidad principal es para listar todos los datos de empleado en la lista de la pestaña de empleado
     * @param lista
     */
    public void listarEmpleado(ArrayList<Empleado> lista){
        vista.dlmEmpleado.clear();
        for(Empleado unempleado : lista){
            vista.dlmEmpleado.addElement(unempleado);
        }
    }

    /**
     * Creamos un array de encargado limpiamos el default list model ç
     * y añadimos elementos a ese default list miodel
     * Su funcionalidad principal es para listar todos los datos de encargado en la lista de la pestaña de encargado
     * @param lista
     */
    public void listarEncargado(ArrayList<Encargado> lista){
        vista.dlmEncargado.clear();
        for(Encargado unencargado : lista){
            vista.dlmEncargado.addElement(unencargado);
        }
    }


    /**
     * Le damos una accion a cada boton ya sea de cada ventana o al desplegable de archivo
     * @param listener
     */
    private void addActionListeners(ActionListener listener){
        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
        vista.btnAltaCliente.addActionListener(listener);
        vista.btnaltaEmpleado.addActionListener(listener);
        vista.btnAltaJuguete.addActionListener(listener);
        vista.altaEncargadoButton.addActionListener(listener);
        vista.btnaltaJugueteria.addActionListener(listener);
        vista.btnaltaProveedor.addActionListener(listener);

        vista.btnBorrarCliente.addActionListener(listener);
        vista.btnborrarEmpleado.addActionListener(listener);
        vista.borrarEncargadoButton.addActionListener(listener);
        vista.btnborrarJuguete.addActionListener(listener);
        vista.btnborrarJugueteria.addActionListener(listener);
        vista.borrarProveedorButton.addActionListener(listener);

        vista.btnModificarCliente.addActionListener(listener);
        vista.btnmodificarEmpleado.addActionListener(listener);
        vista.modificarEncargadoButton.addActionListener(listener);
        vista.btnmodificarJuguete.addActionListener(listener);
        vista.btnmodificarJugueteria.addActionListener(listener);
        vista.btnmodificarProveedor.addActionListener(listener);

        vista.btnListarCliente.addActionListener(listener);

        vista.btnlistarEmpleado.addActionListener(listener);
        vista.listarEncargadosButton.addActionListener(listener);
        vista.btnlistarJuguterias.addActionListener(listener);
        vista.btnlistarJuguetes.addActionListener(listener);
        vista.btnlistarProveedor.addActionListener(listener);

    }

    /**
     * Le añadimos una accion o funcionalidad a cada lista de cada pestaña
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener){
        vista.listClientes.addListSelectionListener(listener);
        vista.listEmpleado.addListSelectionListener(listener);
        vista.listEncargado.addListSelectionListener(listener);
        vista.listJuguete.addListSelectionListener(listener);
        vista.listJuguete.addListSelectionListener(listener);
        vista.listJugueteria.addListSelectionListener(listener);
        vista.listProveedor.addListSelectionListener(listener);

    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     * @param e Evento producido en una lista
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.listClientes) {
                Cliente clienteSeleccion = (Cliente) vista.listClientes.getSelectedValue();
                vista.txtIdCliente.setText(String.valueOf(clienteSeleccion.getId()));
                vista.txtNombreCliente.setText(clienteSeleccion.getNombre());
                vista.txtEmailCliente.setText(clienteSeleccion.getEmail());
                vista.txtDireccionCliente.setText(clienteSeleccion.getDireccion());
                vista.txtTelefonoCliente.setText(clienteSeleccion.getTelefono());


            }else if(e.getSource() == vista.listJuguete){
                    Juguete jugueteSeleccionado = (Juguete) vista.listJuguete.getSelectedValue();
                vista.txtIdJuguete.setText(String.valueOf(jugueteSeleccionado.getId()));
                vista.txtCodigoJuguete.setText(jugueteSeleccionado.getCodigo());
                vista.txtNombreJuguete.setText(jugueteSeleccionado.getNombre());
                vista.txtDescripcionJuguete.setText(jugueteSeleccionado.getDescripcion());
                vista.txtMarcaJuguete.setText(jugueteSeleccionado.getMarca());
                vista.txtPrecioJuguete.setText(String.valueOf(jugueteSeleccionado.getPrecio()));
                vista.fecha.setDate(jugueteSeleccionado.getAnyoCreacion().toLocalDate());

                } else if (e.getSource() == vista.listJugueteria){
                Jugueteria jugueteriaSeleccionado = (Jugueteria) vista.listJugueteria.getSelectedValue();
                vista.txtIdJugueteria.setText(String.valueOf(jugueteriaSeleccionado.getId()));
                vista.txtNombreJugueteria.setText(jugueteriaSeleccionado.getNombre());
                vista.txtCiudadJugueteria.setText(jugueteriaSeleccionado.getCiudad());
                vista.txtPaisJugueteria.setText(jugueteriaSeleccionado.getPais());
                vista.txtTelefonoJugueteria.setText(jugueteriaSeleccionado.getTelefono());

            }else if (e.getSource() == vista.listEmpleado){
                Empleado empleadoSeleccionado = (Empleado) vista.listEmpleado.getSelectedValue();
                vista.txtIdEmpleado.setText(String.valueOf(empleadoSeleccionado.getId()));
                vista.txtNifEmpleado.setText(empleadoSeleccionado.getNif());
                vista.txtNombreEmpleado.setText(empleadoSeleccionado.getNombre());
                vista.txtApellidosEmpleado.setText(empleadoSeleccionado.getApellidos());
                vista.txtTurnoEmpleado.setText(empleadoSeleccionado.getTurno());
                vista.txtDireccionEmpleado.setText(empleadoSeleccionado.getDireccion());
                vista.txtCargoEmpleado.setText(empleadoSeleccionado.getCargo());
                vista.txtTelefonoEncargado.setText(empleadoSeleccionado.getTelefono());


            }else if (e.getSource() == vista.listEncargado){
                Encargado encargadoSeleccionado = (Encargado) vista.listEncargado.getSelectedValue();
                vista.txtIdEncargado.setText(String.valueOf(encargadoSeleccionado.getId()));
                vista.txtNifEncargado.setText(encargadoSeleccionado.getNif());
                vista.txtNombreEncargado.setText(encargadoSeleccionado.getNombre());
                vista.txtApellidosEncargado.setText(encargadoSeleccionado.getApellidos());
                vista.txtAniosEncargado.setText(String.valueOf(encargadoSeleccionado.getAnyosCargo()));
                vista.txtEdadEncargado.setText(String.valueOf(encargadoSeleccionado.getEdad()));
                vista.txtTelefonoEncargado.setText(encargadoSeleccionado.getTelefono());

            }else if (e.getSource() == vista.listProveedor){
                Proveedor proveedorSeleccionado = (Proveedor) vista.listProveedor.getSelectedValue();
                vista.txtIdProveedor.setText(String.valueOf(proveedorSeleccionado.getId()));
                vista.txtCodigoProveedor.setText(proveedorSeleccionado.getCodigoProveedor());
                vista.txtNombreProveedor.setText(proveedorSeleccionado.getNombre());
                vista.txtPaisProveedor.setText(proveedorSeleccionado.getPais());
                vista.txtCiudadProveedor.setText(proveedorSeleccionado.getCiudad());
                vista.txtDireccionProveedor.setText(proveedorSeleccionado.getDireccion());
                vista.txtCantidadProveedor.setText(String.valueOf(proveedorSeleccionado.getCantidad()));
                vista.txtTelefonoProveedor.setText(proveedorSeleccionado.getTelefono());
            }
        }
    }
}
