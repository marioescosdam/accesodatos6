package com.marioea.hibernate.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * Creamos la clase cliente con las variables que reflejamos en la base de datos
 */
@Entity
public class Cliente {
    private int id;
    private String email;
    private String nombre;
    private String direccion;
    private String telefono;
    private List<ClienteJugueteria> visitas;
    private List<ClienteJuguete> compra;

    /**
     * hibernate genera los getters de id
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * hibernate genera los setters de id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * hibernate genera los getters de email
     * @return
     */
    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    /**
     * hibernate genera los setters de email
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * hibernate genera los getters de nombre
     * @return
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    /**
     * hibernate genera los setters de nombre
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * hibernate genera los getters de direccion
     * @return
     */
    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    /**
     * hibernate genera los setters de direccion
     * @param direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * hibernate genera los getters de telefono
     * @return
     */
    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    /**
     * hibernate genera los setters de telefono
     * @param telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return id == cliente.id &&
                Objects.equals(email, cliente.email) &&
                Objects.equals(nombre, cliente.nombre) &&
                Objects.equals(direccion, cliente.direccion) &&
                Objects.equals(telefono, cliente.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, nombre, direccion, telefono);
    }

    /**
     *La primera relacion seria la de cliente con jugueteria
     * en la cual es una relacion (1,N) y con un atributo que es fecha
     * en el medio de la union, entonces lo hacemos a partes mitad y mitad
     * Hacemos la relacion entre cliente y visita primero
     * @return
     */
    @OneToMany(mappedBy = "cliente")
    public List<ClienteJugueteria> getVisitas() {
        return visitas;
    }

    public void setVisitas(List<ClienteJugueteria> visitas) {
        this.visitas = visitas;
    }

    /**
     *La primera relacion seria la de cliente con juguetes
     * en la cual es una relacion (1,N) y con un atributo que es fecha
     * en el medio de la union, entonces lo hacemos a partes mitad y mitad
     * Hacemos la relacion entre cliente y compra primero
     * @return
     */
    @OneToMany(mappedBy = "clientes")
    public List<ClienteJuguete> getCompra() {
        return compra;
    }

    public void setCompra(List<ClienteJuguete> compra) {
        this.compra = compra;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'';
    }
}
