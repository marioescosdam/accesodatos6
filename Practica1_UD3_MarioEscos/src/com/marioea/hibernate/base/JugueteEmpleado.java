package com.marioea.hibernate.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

/**
 * creamos la tabla juguetes_empleados con sus respectivas variables que se encuentran en la bbdd
 */
@Entity
@Table(name = "juguetes_empleados", schema = "jugueteriahibernate", catalog = "")
public class JugueteEmpleado {
    private int id;
    private Date fechaVenta;
    private Juguete juguetes;
    private Empleado empleados;

    /**
     * generamos el getter de id
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * generamos el setter de id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * generamos el getter de fecha_venta
     * @return
     */
    @Basic
    @Column(name = "fecha_venta")
    public Date getFechaVenta() {
        return fechaVenta;
    }

    /**
     * generamos el setter de fecha_venta
     * @param fechaVenta
     */
    public void setFechaVenta(Date fechaVenta) {
        this.fechaVenta = fechaVenta;
    }
    /**
     * te permite saber si dos objetos son iguales
     *
     * @param o objeto introducido para comprobar si es igual
     * @return true si es igual
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JugueteEmpleado that = (JugueteEmpleado) o;
        return id == that.id &&
                Objects.equals(fechaVenta, that.fechaVenta);
    }
    /**
     * te hasea un objeto
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, fechaVenta);
    }
    /**
     * La siguiente relacion que vamos a realizar entre juguete_empleado y juguete en este caso es una relacion(N,M)
     * se realizaria automaticamente la relacion a la hora de mapear.
     * Tanto juguetes y juguete_empleado son varios no es solo uno.
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "id_juguetes", referencedColumnName = "id", nullable = false)
    public Juguete getJuguetes() {
        return juguetes;
    }

    public void setJuguetes(Juguete juguetes) {
        this.juguetes = juguetes;
    }

    /**
     * La siguiente relacion que vamos a realizar entre juguete_empleado y empleados en este caso es una relacion(N,M)
     * se realizaria automaticamente la relacion a la hora de mapear.
     * Tanto empleados y juguete_empleado son varios no es solo uno.
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "id_empleados", referencedColumnName = "id", nullable = false)
    public Empleado getEmpleados() {
        return empleados;
    }

    public void setEmpleados(Empleado empleados) {
        this.empleados = empleados;
    }
}
