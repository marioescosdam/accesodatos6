package com.marioea.hibernate.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * creamos  la tabla jugueteria con sus respectivas variables que se encuentran en la bbdd
 */
@Entity
public class Jugueteria {
    private int id;
    private String nombre;
    private String ciudad;
    private String pais;
    private String telefono;
    private List<ClienteJugueteria> visitas;
    private List<Proveedor> proveedores;
    private List<Empleado> empleados;
    private List<Juguete> juguetes;

    /**
     * generamos el getter de id
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * generamos el setter de id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * generamos el getter de nombre
     * @return
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    /**
     * generamos el setter de nombre
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * generamos el getter de ciudad
     * @return
     */
    @Basic
    @Column(name = "ciudad")
    public String getCiudad() {
        return ciudad;
    }

    /**
     * generamos el setter de ciudad
     * @param ciudad
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * generamos el getter de pais
     * @return
     */
    @Basic
    @Column(name = "pais")
    public String getPais() {
        return pais;
    }

    /**
     * generamos el setter de pais
     * @param pais
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * generamos el getter de telefono
     * @return
     */
    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    /**
     * generamos el setter de telefono
     * @param telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    /**
     * te permite saber si dos objetos son iguales
     *
     * @param o objeto introducido para comprobar si es igual
     * @return true si es igual
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Jugueteria that = (Jugueteria) o;
        return id == that.id &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(ciudad, that.ciudad) &&
                Objects.equals(pais, that.pais) &&
                Objects.equals(telefono, that.telefono);
    }
    /**
     * te hasea un objeto
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, ciudad, pais, telefono);
    }
    /**
     *esta relacion seria la de jugueteria con visitas
     * en la cual es una relacion (1,N) y con un atributo que es fecha
     * en el medio de la union, entonces lo hacemos a partes mitad y mitad
     * Hacemos la relacion entre juguetes y visitas primero
     * @return
     */
    @OneToMany(mappedBy = "jugueteria")
    public List<ClienteJugueteria> getVisitas() {
        return visitas;
    }

    public void setVisitas(List<ClienteJugueteria> visitas) {
        this.visitas = visitas;
    }
    /**
     * La siguiente relacion que vamos a realizar entre juguete_proveedor y jugueteria en este caso es una relacion(N,M)
     * se realizaria automaticamente la relacion a la hora de mapear.
     * Tanto jugueteria y proveedor son varios no es solo uno.
     * @return
     */
    @ManyToMany
    @JoinTable(name = "jugueteria_proveedor", catalog = "", schema = "jugueteriahibernate", joinColumns = @JoinColumn(name = "id_jugueteria", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_proveedor", referencedColumnName = "id", nullable = false))
    public List<Proveedor> getProveedores() {
        return proveedores;
    }

    public void setProveedores(List<Proveedor> proveedores) {
        this.proveedores = proveedores;
    }
    /**
     * La siguiente relacion que vamos a realizar entre juguete_empleados y jugueteria en este caso es una relacion(N,M)
     * se realizaria automaticamente la relacion a la hora de mapear.
     * Tanto jugueteria y empleados son varios no es solo uno.
     * @return
     */
    @ManyToMany
    @JoinTable(name = "jugueteria_empleados", catalog = "", schema = "jugueteriahibernate", joinColumns = @JoinColumn(name = "id_jugueteria", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_empleados", referencedColumnName = "id", nullable = false))
    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }

    /**
     * La siguiente relacion que vamos a realizar entre juguete_juguetes y juguetes en este caso es una relacion(N,M)
     * se realizaria automaticamente la relacion a la hora de mapear.
     * Tanto jugueteria y juguetes son varios no es solo uno.
     * @return
     */
    @ManyToMany
    @JoinTable(name = "jugueteria_juguetes", catalog = "", schema = "jugueteriahibernate", joinColumns = @JoinColumn(name = "id_jugueteria", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_juguetes", referencedColumnName = "id", nullable = false))
    public List<Juguete> getJuguetes() {
        return juguetes;
    }

    public void setJuguetes(List<Juguete> juguetes) {
        this.juguetes = juguetes;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", ciudad='" + ciudad + '\'' +
                ", pais='" + pais + '\'';
    }
}
